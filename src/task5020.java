import java.util.Arrays; //task3
import java.util.List;
import java.util.ArrayList; //task5
import java.util.*;


public class task5020 {
    public static void main(String[] args) throws Exception {
        task5020 task = new task5020();

        String str = "Devcamp";
        boolean vCheck1 = task5020.isArray(str);
        System.out.print("Co phai la mang khong: " + vCheck1 + "\n");
        int []arr1 = {1, 2, 3};
        boolean vCheck2 = task5020.isArray(arr1);
        System.out.print("Co phai la mang khong: " + vCheck2 + "\n");

        //task 2
        int []arr2 = {1,2,3,4,5,6};
        System.out.print("Giá trị phan tu : " + arr2[3] + "\n");

        //task 3
        int []arr3 = {3,8,7,6,5,-4,-3,2,1};
        Arrays.sort(arr3);
        //In kết quả mảng sau khi sắp xếp
        System.out.println("Mang sau khi da sap xep la: " + Arrays.toString(arr3) + "\n"); 

        //task4        
        int elem;
        elem = task5020.returnElementOfArray(7, arr2);
        System.out.print("Phan tu dung vi tri thu may trong mang la: " + elem + "\n");
        
        //task 5
        //khai báo và khởi tạo cho hai mảng array1 và array2
        int[] array1 = {1, 2, 3};
        int[] array2 = {4, 5, 6};
        //aLen sẽ bằng độ dài của mảng array1 và bLen sẽ bằng độ dài của mảng array2
        int aLen = array1.length;
        int bLen = array2.length;
        //khai báo một mảng result với độ dài bằng aLen + bLen để lưu trữ các phần tử của array1 và array2
        int[] result = new int[aLen + bLen];
        //sử dụng phương thức arraycopy() để copy các phần tử của array1 và array2 vào result
        System.arraycopy(array1, 0, result, 0, aLen);
        System.arraycopy(array2, 0, result, aLen, bLen);
        //sử dụng phương thức toString() để hiển thị mảng ra màn hình
        System.out.println("Ket qua cua viec gop mang la: " + Arrays.toString(result) + "\n");

        //task 6
        Object[] arrTask6 = {Double.NaN, 0, 15, false, -22, "", null, 47, null};
        List<Object> newArr = new ArrayList<>();
        for (Object obj : arrTask6) {
            if (obj instanceof Number || obj instanceof String) {
                newArr.add(obj);
            }
        }
        Object[] vResult = newArr.toArray();
        System.out.println("Mang da kieu sau khi loc xong: " + Arrays.toString(vResult) + "\n");

        //task 7
        int[] arrTask7 = {2, 5, 9, 6};
        int n = 5;
        arrTask7 = Arrays.stream(arrTask7).filter(val -> val != n).toArray();
        System.out.println("Mang sau khi remove phan tu n: " + Arrays.toString(arrTask7) + "\n");

        //task 8
        int[] arr = {2, 5, 9, 6};
        int randomIndex = new Random().nextInt(arr.length);
        int randomElement = arr[randomIndex];
        System.out.println("Lay random cua 1 phan tu bat ky trong mang: " + randomElement + "\n");

        //task 9
        int x = 4;
        int y = 11;
        int[] arrTask9 = new int[x];
        Arrays.fill(arrTask9, y);
        System.out.println("Mang co x phan tu gia tri y: " + Arrays.toString(arrTask9) + "\n");

        //task 10
        int x10 = -6;
        int y10 = 4;
        int[] arrTask10 = new int[y10];
        for (int i = 0; i < y10; i++) {
            arrTask10[i] = x10 + i;
        }
        System.out.println("Tao mot mang gom y so lien tiep bat dau tu gia tri x" + Arrays.toString(arrTask10));
    }

    //task1
    public static boolean isArray(Object obj){
        return obj.getClass().isArray();    
    }
    //task4
    public static int returnElementOfArray(int number, int []arr) {
        int element = -1;
        for (int i = 0; i < arr.length; i++) {
            if(number == arr[i]){
                element = i;
            }
        }
        return element;
    }
    
}
